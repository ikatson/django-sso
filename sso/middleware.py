"""
Single Sign On Middleware
"""
import logging
import time

from django import http
from django.conf import settings
from django.contrib.auth import login
from django.contrib.auth.models import User
from django.contrib.sites.models import Site

from sso.util import generate_sso_token

log = logging.getLogger(__name__)


class SingleSignOnMiddleware(object):
    """
    checks for sso token and logs user in
    checks for external urls to change
    """
    def __init__(self):
        try:
            self.timeout = settings.SSO_TIMEOUT
        except AttributeError:
            self.timeout = 1
        try:
            self.protocol = settings.SSO_PROTOCOL
        except AttributeError:
            self.protocol = 'http://'
        self.seen_nonces = []

    def process_request(self, request):
        token = request.GET.get('token', False)
        id_ = request.GET.get('id', False)
        timestamp = request.GET.get('timestamp', False)
        nonce = request.GET.get('nonce', None)
        if token and id and timestamp and nonce:
            if self.check_token(token, id_, timestamp, nonce):
                # everything passed, authenticate user
                try:
                    user = self.authenticate(id_)
                except User.DoesNotExist:
                    log.debug('User with id %s does not exist, '
                              'ignoring valid SSO', id_)
                    raise http.Http404
                login(request, user)
        return None

    def check_token(self, token, id_, timestamp, nonce):
        """
        checks the token based on id, timestamp, and sso secret
        """
        timediff = time.time() - float(timestamp)
        if timediff >= self.timeout:
            log.debug('Time difference %s is more than timeout %s, '
                      'rejecting SSO', timediff, self.timeout)
            return False
        if nonce in self.seen_nonces:
            log.debug('Nonce already seen, rejecting SSO')
            return False
        if token == generate_sso_token(id_, timestamp, nonce):
            self.seen_nonces.append(nonce)
            self.seen_nonces = self.seen_nonces[-200:]
            return True

    def authenticate(self, id_):
        """
        go through the backends to find the user
        same as django.contrib.auth.authenticate but doesn't need a password
        """
        from django.contrib.auth import get_backends
        for backend in get_backends():
            try:
                user = backend.get_user(id_)
            # pylint: disable=W0702
            # We do not know DoesNotExist exception subtype.
            except:
                # didn't work, try the next one.
                continue
            if user is None:
                continue
            # Annotate the user object with the path of the backend.
            user.backend = "%s.%s" % (
                backend.__module__, backend.__class__.__name__)
            return user
        else:
            raise User.DoesNotExist(id_)

    def process_response(self, request, response):
        """ takes the response output and replaces urls """
        try:
            if request.user.is_authenticated():
                try:
                    domains = settings.SSO_DOMAINS
                    if domains:
                        response.content = self.replace_domain_urls(
                            response.content, domains)
                except AttributeError:
                    pass
        except AttributeError:
            # in case request.user doesn't exist
            pass
        return response

    def replace_domain_urls(self, content, domains):
        """
        Replaces urls for domains specified and replaces them with
        a url to the sso view that will generate a token and redirect
        """
        current_domain = Site.objects.get_current().domain
        for domain in domains:
            if not (domain.startswith('http://') and
                    not domain.startswith('https://')):
                domain = 'http://' + domain
            content = content.replace(domain, '%s%s/sso/?next=%s' % (
                self.protocol,
                current_domain,
                domain
            ))
        return content
