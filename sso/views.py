import logging
import random
import string
import time

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect

from sso.util import generate_sso_token

log = logging.getLogger(__name__)

srandom = random.SystemRandom()

@login_required
def sso(request, default_url='/'):
    assert settings.SSO_WHITELIST_NEXT_URL_CALLBACK
    sso_check_next_url_func = settings.SSO_WHITELIST_NEXT_URL_CALLBACK
    try:
        next = sso_check_next_url_func(
            request.user, request.GET.get('next'))
    except:
        log.exception('Error executing next callback', sso_check_next_url_func)
        next = None
    if not next:
        return HttpResponseRedirect(default_url)

    id_ = request.user.id
    timestamp = time.time()
    nonce = ''.join(srandom.choice(string.letters)
                    for i in xrange(10))
    token = generate_sso_token(id_, timestamp, nonce)
    url = "%s?id=%s&timestamp=%s&token=%s&nonce=%s" % (
        next, id_, timestamp, token, nonce)
    return HttpResponseRedirect(request.build_absolute_uri(url))
