import hmac

from django.conf import settings


def generate_sso_token(id_, timestamp, nonce):
    token = hmac.new(settings.SSO_SECRET, "%s%s%s" % (id_, timestamp, nonce))
    return token.hexdigest()
