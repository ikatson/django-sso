#!/usr/bin/env python

from setuptools import setup, find_packages

setup(name='django-sso',
      version='0.0.8',
      description='django app for SSO login on several sites',
      author='pyroman@gmail.com',
      author_email='pyroman@gmail.com',
      url='https://bitbucket.org/descent/django-sso',
      packages=find_packages(),
      classifiers=[
          'Development Status :: 3 - Alpha',
          'Intended Audience :: Developers',
          'License :: OSI Approved :: GNU General Public License (GPL)',
          'Operating System :: POSIX',
          'Programming Language :: Python',
          'Topic :: Software Development :: Libraries :: Python Modules',
          ],
     )
