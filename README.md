Installation
------------

1. SERVER: set settings.SSO_WHITELIST_NEXT_URL_CALLBACK to a function, accepting
   the current user, and the provided next url.

   Return the URL, that the user should be redirected to, if everything is ok,
   or None, if it's not.

2. CLIENT: insert sso.middleware.SSOMiddleware into middleware.